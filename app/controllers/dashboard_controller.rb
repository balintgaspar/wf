require 'net/http'


class DashboardController < ApplicationController
    def index
        @title = "Watchfit Dashboard"
        @buddy = "assets/slide_image.png"
        @username = "Gaspar Balint"
        @user_points = 2500
    end
    
    def stats
        render json: Stats.all
    end
end
