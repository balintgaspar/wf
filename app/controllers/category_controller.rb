class CategoryController < ActionController::Base
        def show
            @cat = params[:id]
            if @cat != 'all'
                uri = URI('http://watchfitblog.okami.hu/?json=get_category_posts&slug='+@cat)
                @posts = Net::HTTP.get(uri)
                @posts = JSON.parse(@posts)
                @posts = @posts['posts']
                render :json => @posts
            else
                posts_uri = URI('http://watchfitblog.okami.hu/?json=1')
                @posts = Net::HTTP.get(posts_uri)
                @posts = JSON.parse(@posts)
                @posts = @posts['posts']
                render :json => @posts
            end
        end
    
        def search
            
        end
end