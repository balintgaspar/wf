require 'net/http'
require 'cgi'
require 'uri' 

class ArticlesController < ApplicationController
    def index
        
        
        categories_uri = URI('http://watchfitblog.okami.hu/?json=get_category_index')
        @cat = Net::HTTP.get(categories_uri)
        @cat = JSON.parse(@cat)
        @cat = @cat['categories']
        
    end
    
    def post
        @id = params[:id]
        uri = URI('http://watchfitblog.okami.hu/?json=1&p='+@id)
        @posts = Net::HTTP.get(uri)
        @posts = JSON.parse(@posts)
        @posts = @posts['post']
    end
    
    

end
