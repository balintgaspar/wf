$(document).ready(function(){

if($('body').hasClass('dashboard_controller'))
{
   $('.dashboard-tabs').wftabs({
        tabcontents: $('.dashboard-tabs-content'),
       effect:"accordion",
       speed: 400,
    });
    
     $('.stat-tabs').wftabs({
        tabcontents: $('.stat-container'),
       effect:"fade",
       tab_content_class:".stat",
       speed: 400,
    });  
    
    
    $('.wide-container.tabs-container').scrollToFixed({
        marginTop: 72,
        zIndex: 10
    });
    
 
    
    
    
    
    
    $('#accordion').accordion({
        
        header:'.acc-header',
        heightStyle: 'content',
        activate: function(event,ui)
        {
            
            $('.calendar-container').animate({
            
                'height': $('.details').height(),
            
            },100);
        }
    
    });
 
    
    
    $('.calendar-container').animate({
            
                'height': $('.details').height(),
            
            },100);
                           
}  
});

