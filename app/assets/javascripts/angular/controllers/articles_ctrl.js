var app = app || angular.module('watchfit');

$(function(){

app.controller('ArticleCtrl', function($scope,Category) {
 
    $scope.items = Category.query({id: 'all'});
    
    $scope.loadAll = function()
    {
           $scope.items = Category.query({id: 'all'});
    }
    
    $scope.loadCategory = function(slug)
    {
        
        $scope.items = Category.query({id: slug});  
        
    }
    
});

})
