'use strict';
 
var app = angular.module('watchfit');

app.factory('Category', function($resource) {
    
    return $resource('/category/:id',{id: '@id'});
});
