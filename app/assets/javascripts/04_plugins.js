$(function(){
    
$.fn.wftabs = function(options){
    
    var settings = $.extend({
    
        tabcontents:$('#tab-contents'),
        effect: "fade" ,
        speed: 500,
        tab_content_class:".tab-content"
    
    },options);
    
    var tabbar = this;
    
    var curr_tab = this.find('li.active').index();
    
    settings.tabcontents.find(settings.tab_content_class).each(function () {
        $(this).css('display', 'none')
    }).eq(curr_tab).css('display', 'block');
    
    
    this.find('li').each(function(){
    
        
        
        $(this).click(function(){
           
            if(!$(this).hasClass('active'))
            {
                $(tabbar).find('li').removeClass('active');
                
                $(this).addClass('active');
                curr_tab = $(this).index();
                
                if(settings.effect == "fade")
                {
                    settings.tabcontents.find(settings.tab_content_class).css('display','none').eq(curr_tab).fadeIn(1000);
                }
                else if(settings.effect == "accordion")
                {
                    settings.tabcontents.find(settings.tab_content_class).slideUp(settings.speed).eq(curr_tab).slideDown(settings.speed);
                }
                else if(settings.effect == "slide")
                {
                    //here is the slide effect                    
                }
            
            }
                
        });
    
    });
    
    
    return this;

}

    

})