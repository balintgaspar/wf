var start = "M119.259,61.646c0,18.575-9.411,34.951-23.725,44.622c-4.789,3.235-10.126,5.72-15.843,7.285 c-4.537,1.242-9.313,1.905-14.244,1.905c-5.063,0-9.962-0.699-14.607-2.006c-5.264-1.481-10.201-3.743-14.678-6.653 c-14.763-9.595-24.527-26.234-24.527-45.153c0-29.72,24.093-53.812,53.812-53.812S119.259,31.926,119.259,61.646z";

var fin = "M118.624,61.646c0,18.575-9.411,34.951-23.725,44.622 c-4.789,3.235-37.487,27.019-23.803,70.892c3.154,7.591-0.346,12.341-6.178,12.341c-6.168,0-8.168-5.5-5.392-12.552 c11.96-41.878-19.521-67.239-23.999-70.149C20.764,97.204,11,80.565,11,61.646c0-29.72,24.093-53.812,53.812-53.812 S118.624,31.926,118.624,61.646z";

var color_speed = 300;

$(document).ready(function () {
    
    $('.slider').scrollToFixed({
        marginTop: 68,
        zIndex: 10
    });
    
    if($('.tabs').hasClass('home'))
    {
        initHomeTabs();
        $('.tabs').wftabs({
            tabcontents: $('.tab-content-container'),
            effect: "fade",
            speed: 1000
        });
        $('.tab1').addClass('active');
    }
   
    
    
    $(".slide .container h1").fitText(1.2, {
        minFontSize: '20px',
        maxFontSize: '60px'
    })
});




var initHomeTabs = function () {
    
    var r1 = Raphael("svg1", 130, 220),
        pos = [0, 0];
    var r2 = Raphael("svg2", 130, 220),
        pos = [0, 0];
    var r3 = Raphael("svg3", 130, 220),
        pos = [0, 0];
    
    var nutritions = r1.path(start).attr({
        fill: "#1793fb",
        "stroke-width": 0
    });
    var workout = r2.path(start).attr({
        fill: "#1793fb",
        "stroke-width": 0
    });
    var game = r3.path(start).attr({
        fill: "#1793fb",
        "stroke-width": 0
    });
    
    
    
    
    
    var active = true;
    
    function killPaths() {
        
        nutritions.attr('fill', "#1793fb");
        workout.attr('fill', "#1793fb");
        game.attr('fill', "#1793fb");
        nutritions.animate({
            path: start
        }, 400, "backIn");
        workout.animate({
            path: start
        }, 400, "backIn");
        game.animate({
            path: start
        }, 400, "backIn");
        
        
        
    }
    
    setTimeout(function () {
        
        nutritions.animate({
            path: fin,
            fill: "#8ccc1c"
        }, 800, "elastic");
        
    }, 400);
    
    
    $('.tab1').click(function () {
        
        killPaths();
        
        nutritions.animate({
            path: fin
        }, 800, "elastic");
        nutritions.animate({
            fill: "#8ccc1c"
        }, color_speed, "linear");
        active = true;
        
        
    })
    
    $('.tab2').click(function () {
        
        killPaths();
        workout.animate({
            path: fin
        }, 800, "elastic");
        workout.animate({
            fill: "#8ccc1c"
        }, color_speed, "linear");
        active = true;
        
        
    })
    
    $('.tab3').click(function () {
        
        killPaths();
        game.animate({
            path: fin
        }, 800, "elastic");
        game.animate({
            fill: "#8ccc1c"
        }, color_speed, "linear");
        active = true;
       
        
    })
    
}

